import cv2
import os
import enum
import time
import numpy as np

class State(enum.Enum):
    PRE_START = 0
    START = 1
    FIRST_STRIPE = 2
    START_WHITE = 3
    SECOND_STRIPE = 4
    LEFT_DATA_WHITE = 5
    LEFT_DATA_BLACK = 6
    END = 7
    
# Function that checks if the count of pixels can represent the desired width bar width, given the number of pixels for 1 unit.
# INPUT: 
#   count: nr of pixels of same color
#   unit: width of one unit in number of pixels
#   width: the bar width in the barcode that is compared against.
# OUTPUT:
#   True if is in limits
#   False if not in limits
def check_stripe_valid(count, unit, width):
    if (abs(width * unit - count) < 0.47*unit):
        return True
    return False
    
# Function that determines how many barcode units the seen bar is wide. If there is no definite corraspondance or is outside limits then 0 is returned.
# INPUT: 
#   count: nr of pixels of same color
#   unit: width of one unit in number of pixels
# OUTPUT:
#   Bar width in units as integer from 1 to 4 if there is definite matching width.
#   False if not in limits for any of 1,2,3,4 unit widths
def get_stripe_width(count, unit):
    for width in range(1, 5):
        if check_stripe_valid(count, unit, width):
            return width
    return False
    
# Function that decodes a symbol after 4 bars of altering colors have been found. The match is checked against a dictionary. As a special case an end symbol can be determined. This is noted with a -1 as output state.
# INPUT: 
#   symbol - list of 4 bar widths in barcode units that have been detected. This is compared against predefined dictionary in both orders to see if there is odd match or even match. 
#   decoded_symbols - list that holds already decoded symbols. If a match is found for symbol, then it is added to that list.
#   value_dict - predefined reference dictionary, containing all valid bar width combinations and the corresponding digits from 0 to 9.
#   total_sym_pixels - number of total pixels for this symbol. this is used to calculate new barcode unit width in case it has changed because of tilted barcode.
# OUTPUT:
#   tuple of 4 elements, containing:
#   1 - result status - true if decoding was successful and a valid data digit was decoded, -1 when end sequence was found and 0 when there was failure.
#   2 - boolean, showing if an odd digit sequence was detected. Only valid when a data digit was succesfully decoded. Forced to 0 otherwise.
#   3 - boolean, showing if an even digit sequence was detected. Only valid when a data digit was succesfully decoded. Forced to 0 otherwise.
#   4 - new barcode unit width, derived from the total_sym_pixels input. Only valid when a data digit was succesfully decoded. Forced to 0 otherwise.
def decode_symbol(symbol, decoded_symbols, value_dict, total_sym_pixels):
    symbol_tuple = tuple(symbol)
    inv_symbol_tuple = tuple(symbol[::-1])
    #print(symbol)
    if symbol_tuple in value_dict:
        decoded_symbols.append(value_dict[symbol_tuple])
        return (True, 1, 0, total_sym_pixels/7.0) 
    elif inv_symbol_tuple in value_dict:
        decoded_symbols.append(value_dict[inv_symbol_tuple])
        return (True, 0, 1, total_sym_pixels/7.0)
    elif symbol_tuple == (1,1,1,1):
        #print("aeae")
        return (-1, 0, 0, 0)
    return (False, 0, 0, 0) #Make also return false when starts with wrong color.
    
# Function that converts a list of digits into a single base-10 number.
# INPUT: 
#   num_list - list of individual digits in a detected barcode.
# OUTPUT:
#   6-digit integer number from the input digit list.
def code_from_num_list_to_int(num_list):
    num_string = ''.join(map(str, num_list))
    return int(num_string)

# Main function of decoding a barcode in a row of pixels.
# The logic is implemented as a state machine which keeps the status of which part of the barcode is currently being detected.
# Each new pixel that is read causes a transition.
# Actions are taken on transitions.
def scan_row(rida):
    white_count = 0
    black_count = 0
    start_count = 0
    unit_dimension = 0
    even_cnt = 0
    odd_cnt = 0
    total_sym_pixels = 0

    symbol = []
    decoded_symbols = []
    state = State.PRE_START
    row_found_barcodes = []

    value_dict = {(3,2,1,1): 0,
                  (2,2,2,1): 1,
                  (2,1,2,2): 2,
                  (1,4,1,1): 3,
                  (1,1,3,2): 4,
                  (1,2,3,1): 5,
                  (1,1,1,4): 6,
                  (1,3,1,2): 7,
                  (1,2,1,3): 8,
                  (3,1,1,2): 9}



    start_end_elem = 0

    for n, elem in enumerate(rida):
        #print(state)
        if state == State.PRE_START:
            if elem:
                white_count = 1
                state = State.START
        if state == State.START:
            if elem:
                white_count += 1
            else:
                state = State.FIRST_STRIPE
                black_count = 1
        elif state == State.FIRST_STRIPE:
            if elem:
                if black_count < white_count * 7:
                    state = State.START_WHITE
                    unit_dimension = black_count
                    start_count = black_count
                else:
                    state = State.START
                white_count = 1
            else:
                black_count += 1
        elif state == State.START_WHITE:
            if elem:
                white_count += 1
            else:
                black_count = 1
                if check_stripe_valid(white_count, unit_dimension, 1):
                    state = State.SECOND_STRIPE
                    start_count += white_count
                else:
                    state = State.START
                white_count = 1
        elif state == State.SECOND_STRIPE:
            if elem:
                white_count = 1
                if check_stripe_valid(black_count, unit_dimension, 1):
                    state = State.LEFT_DATA_WHITE
                    start_count += black_count
                    unit_dimension = start_count / 3.0
                else:
                    state = State.START
                black_count = 1
            else:
                black_count += 1
        elif state == State.LEFT_DATA_WHITE:
            if elem:
                white_count += 1
            else:
                black_count = 1
                result_width = get_stripe_width(white_count, unit_dimension)
                if result_width:
                    state = State.LEFT_DATA_BLACK
                    symbol.append(result_width)
                    total_sym_pixels += white_count
                    if len(symbol) >= 4:
                        symbol_decode_result, odd_inc, even_inc, new_unit = decode_symbol(symbol, decoded_symbols, value_dict, total_sym_pixels)
                        if new_unit:
                            unit_dimension = new_unit
                        total_sym_pixels = 0
                        symbol = []
                        even_cnt += even_inc
                        odd_cnt += odd_inc
                        if symbol_decode_result == -1:
                            state = State.END
                        elif symbol_decode_result == 0:
                            state = State.START #possibly change that
                    
                else:
                    state = State.START #Change that
        elif state == State.LEFT_DATA_BLACK:
            if elem:
                white_count = 1
                result_width = get_stripe_width(black_count, unit_dimension)
                if result_width:
                    state = state.LEFT_DATA_WHITE
                    symbol.append(result_width)
                    total_sym_pixels += black_count
                    if len(symbol) >= 4:
                        symbol_decode_result, odd_inc, even_inc, new_unit = decode_symbol(symbol, decoded_symbols, value_dict, total_sym_pixels)
                        if new_unit:
                            unit_dimension = new_unit
                        total_sym_pixels = 0
                        symbol = []
                        even_cnt += even_inc
                        odd_cnt += odd_inc
                        if symbol_decode_result == -1:
                            state = State.END
                        elif symbol_decode_result == 0:
                            state = State.START #possibly change that
                    
                else:
                    state = State.START #Change that
            else:
                black_count += 1
        elif state == State.END:
            if even_cnt == 3 and odd_cnt == 3:
                #print("correct")
                row_found_barcodes.append((code_from_num_list_to_int(decoded_symbols), row_number, n, unit_dimension))
            decoded_symbols = []
            odd_cnt = 0
            even_cnt = 0
            white_count = 0
            black_count = 0
            unit_dimension = 0
            start_count = 0
            state = State.PRE_START
    return row_found_barcodes.copy()
    
cap = cv2.VideoCapture(0)

# Main loop, doing the video handling and for each frame calling the detection method. Testing and demo purpose.
while True:
    ret, frame = cap.read()
    img_org = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    found_barcodes = []

    #img_org = cv2.imread("1_0727272.png", 0)
    #img_org = cv2.imread("0223399_bigger.png", 0)
    #img_org = cv2.imread("real1.jpg", 0)

    ret, img = cv2.threshold(img_org, 180, 255, cv2.THRESH_BINARY)
    cv2.imwrite("temp_thresh.png", img)

    picture_height = img.shape[0]

    start = time.time()
    for row_number in range(0, picture_height, 60):
        rida = img[row_number]
        found_barcodes.extend(scan_row(rida))

    end = time.time()
    found_barcodes_dict = {}

    #Combining the information from individual rows to find the location of each individual barcode that are seen on the frame.
    for id, vert, hor, unit in found_barcodes:
        if id in found_barcodes_dict:
            existing_dict = found_barcodes_dict[id]
            existing_dict["cnt"] += 1
            if vert > existing_dict["vert_max"]:
                existing_dict["vert_max"] = vert
            elif vert < existing_dict["vert_min"]:
                existing_dict["vert_min"] = vert
            existing_dict["hor_ave"] = existing_dict["hor_ave"] + (hor - existing_dict["hor_ave"])/existing_dict["cnt"]
            existing_dict["unit_ave"] = existing_dict["unit_ave"] + (unit - existing_dict["unit_ave"])/existing_dict["cnt"]
        else:
            found_barcodes_dict[id] = {"cnt":1.0, "vert_max": vert, "vert_min": vert, "hor_ave": hor, "unit_ave": unit}



    #print(found_barcodes)
    #print("\n\n", found_barcodes_dict)
    #print("\n\n", end-start)
    
    for id in found_barcodes_dict:
        value = found_barcodes_dict[id]
        cv2.rectangle(frame, (int(value["hor_ave"]-50*value["unit_ave"]),int(value["vert_min"] - 30)), ( int(value["hor_ave"] + 10),int(value["vert_max"] + 30)), (30, 170, 30), thickness=2)
        cv2.putText(frame, str(id), (int(value["hor_ave"]-47*value["unit_ave"]), int(value["vert_min"] - 10 - value["unit_ave"] * 10)), cv2.FONT_HERSHEY_SIMPLEX, value["unit_ave"] / 5.0, (30, 170, 30), 2)

    #cv2.line(img_org, (start_end_elem,0), (start_end_elem,60), 125, 1)
    cv2.imshow("image", frame)


    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        cap.release()
        break
